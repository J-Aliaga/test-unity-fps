﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorDeArmas : MonoBehaviour
{
    public LogicaArma[] armas;
    private int indiceArmasActual = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RevisarCambioDeArma();
    }

    void CambiarArmaActual()
    {
        for (int i = 0; i< transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        armas[indiceArmasActual].gameObject.SetActive(true);
    }

    void RevisarCambioDeArma()
    {
        float ruedaMouse = Input.GetAxis("Mouse ScrollWheel");
        if (ruedaMouse > 0f)
        {
            SeleccionarArmaAnterior();
            armas[indiceArmasActual].recargando = false;
            armas[indiceArmasActual].tiempoNoDisparo = false;
            armas[indiceArmasActual].estaADS = false;
        }
        else if (ruedaMouse < 0f)
        {
            SeleccionarArmaSiguiente();
            armas[indiceArmasActual].recargando = false;
            armas[indiceArmasActual].tiempoNoDisparo = false;
            armas[indiceArmasActual].estaADS = false;
        }
    }

    void SeleccionarArmaAnterior()
    {
        if (indiceArmasActual == 0)
        {
            indiceArmasActual = armas.Length - 1;
        }
        else
        {
            indiceArmasActual--;
        }

        CambiarArmaActual();
    }

    void SeleccionarArmaSiguiente()
    {
        if (indiceArmasActual >= (armas.Length -1))
        {
            indiceArmasActual = 0;
        }
        else
        {
            indiceArmasActual++;
        }

        CambiarArmaActual();
    }
}
